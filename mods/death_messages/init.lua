--[[
death_messages - A Minetest mod which sends a chat message when a player dies.
Copyright (C) 2016  EvergreenTree

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
--]]

-----------------------------------------------------------------------------------------------
local title = "Death Messages"
local version = "0.1.2"
local mname = "death_messages"
-----------------------------------------------------------------------------------------------
dofile(minetest.get_modpath("death_messages").."/settings.txt")
-----------------------------------------------------------------------------------------------

-- A table of quips for death messages.  The first item in each sub table is the
-- default message used when RANDOM_MESSAGES is disabled.
local messages = {}

-- Lava death messages
messages.lava = {
	" took a trip to a volcano.",
	" wanted to meet the sun in person.",
	" was met with a molten demise.",
	" looked for fire and failed.",
	" dug straight down.",
	" touched something they were not supposed to.",
        " saw something bright and got too close."
}

-- Drowning death messages
messages.water = {
	" was slain by water using water.",
	" ran out of air.",
	" failed at swimming lessons.",
	" tried to impersonate an anchor.",
	" went swimming with the fishes.",
	" tried amateur scuba-diving.",
        " realized they lacked gills."
}

-- Burning death messages
messages.fire = {
	" burned to a crisp.",
	" tried to fry eggs.",
	" was starving and cooked themself.",
	" just got roasted, hotdog style.",
	" gout burned up. More light that way.",
        " wanted to be an arsonist.",
        " tried to use magic spells.",
        " became too hot.",
        " went up in fire.",
        " was too poor to afford a barbeque.",
        " was outmatched by an unholy wrath.",
        " died from trying to cook by hand.",
        " spilled a hot beverage.",
        " was killed by a hot beverage.",
        " tried to perform a science experiment."
}

-- Other death messages
messages.other = {
	" died.",
	" did something fatal.",
	" gave up on life.",
	" is somewhat dead now.",
	" passed out -permanently.",
        " was slain.",
        " was killed.",
        " finished their bucket list.",
        " was killed by an unknown force.",
        " took the easy way out.",
        " met with the Grim Reaper.",
        " did not try hard and did not get very far.",
        " tried to leave the game.",
        " wanted to leave the simulation but failed.",
        " finally understood what karma meant.",
        " took things too seriously.",
        " bit the dust.",
        " was tonight's special entree.",
        " was in Thanatos' grasp.",
        " was on the dinner menu.",
        " had been served!",
        " tried to escape but failed.",
        " got their end of the bargain.",
        " died by something.",
        " did not want to be alive anymore.",
        " un-alive'd.",
        " did something bad.",
        " did not believe in God hard enough.",
        " had not prayed enough for their life!",
        " was not an immortal.",
        " tested themselves and died.",
        " had a tantrum.",
        ", you lose!",
        " lost the game.",
        " don't live, the game does!",
        "... GAME OVER, MAN; GAME OVER!",
        " wanted to see how dying felt.",
        " stubbed their toe.",
        " stood in awe of perfectionism.",
        " closed their eyes and went to sleep forever.",
        " lost their footing.",
        " saw something grim and collapsed.",
        " collapsed.",
        "'s legs broke.",
        "'s legs fell off.",
        "'s legs had a mind of its own.",
        "'s legs fell asleep.",
        "'s eyes saw darkness right away.",
        " stopped breathing and fell down.",
        " froze in place and died.",
        " gave one final breath.",
        " was stubborn.",
        " was clumsy.",
        " met with a terrible fate.",
        " was no longer a part of this world.",
        " wants to be in a cemetary.",
        " ended up dead.",
        " perished.",
        " could not accept reality.",
        "'s heart stopped working.",
        " is not responding.",
        " turned into a pile of bones.",
        " went down.",
        " tried to find the meaning of life.",
        " died from boredom.",
        " died from not living.",
        " stopped living.",
        " wanted to mock a blind person.",
        " stole from an old person and got something in return.",
        " tried to outrun the police.",
        " wanted to live forever and failed.",
        " had an affair with death.",
        " is dead.",
        " had their last grain of sand fall through the hourglass.",
        " thought they sneezed.",
        " tried to hold in a sneeze."
}

function get_message(mtype)
	if RANDOM_MESSAGES then
		return messages[mtype][math.random(1, #messages[mtype])]
	else
		return messages[1] -- 1 is the index for the non-random message
	end
end

minetest.register_on_dieplayer(function(player)
	local player_name = player:get_player_name()
	local node = minetest.registered_nodes[minetest.get_node(player:getpos()).name]
	if minetest.is_singleplayer() then
		player_name = "You"
	end
	-- Death by lava
	if node.groups.lava ~= nil then
		minetest.chat_send_all(player_name .. get_message("lava"))
	-- Death by drowning
	elseif player:get_breath() == 0 then
		minetest.chat_send_all(player_name .. get_message("water"))
	-- Death by fire
	elseif node.name == "fire:basic_flame" then
		minetest.chat_send_all(player_name .. get_message("fire"))
	-- Death by something else
	else
		minetest.chat_send_all(player_name .. get_message("other"))
	end

end)

-----------------------------------------------------------------------------------------------
print("[Mod] "..title.." ["..version.."] ["..mname.."] Loaded...")
-----------------------------------------------------------------------------------------------
