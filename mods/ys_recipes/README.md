## ys_recipes

**ys_recipes** adds a variety of crafting and furnace recipes, with some including the ability to create non-renewing materials with other materials that can be renewed. This is primarily aimed at minetest_game.

For example, one important ingredient in crafting may be `default:steel_ingot` since it is a popularized and widely used material. For this reason, `default:iron_lump` is a craftable item (see below.)

```
default:flint, default:flint, default:flint
default:flint, bucket:bucket_water, default:flint
default:flint, default:flint, default:flint
```

When combined, grants 3 of `default:iron_lump`.

Future releases may have this recipe changed; though as of **10-14-2022**, this is the current recipe.
