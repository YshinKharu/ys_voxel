--[[
        ys_recipes
        YshinKharu - 10-09-2022

        Makes non-renewable items craftable for minetest_game.
        Includes several recipes using flint.

	GitLab: https://gitlab.com/YshinKharu/ys_voxel

	Code is not final; expect unprofessionalism & bugs.
        Licensed under ICCLEIYSIUYA.
        http://www.evvk.com/evvktvh.html
--]]


fli = "default:flint"
jgr = "default:junglegrass"
g_l = "group:leaves"

--
-- shapeless
--
--[[
minetest.register_craft(
{
        output = "farming:seed_wheat", -- get seeds from grass instantly, not by chance
        recipe = {
                {"default:grass_1"}
}
})

minetest.register_craft(
{
        output = "farming:seed_cotton",
        recipe = {
                {"default:junglegrass"}
}
})

--]]

minetest.register_craft({
        type = "shapeless",
        output = "default:ice",
        recipe = {
                "default:snowblock",
                "bucket:bucket_water"
        },
        replacements = {{"bucket:bucket_water", "bucket:bucket_empty"}}
})

minetest.register_craft({
        type = "shapeless",
        output = "default:ice",
        recipe = {
                "default:snowblock",
                "bucket:bucket_river_water"
        },
        replacements = {{"bucket:bucket_river_water", "bucket:bucket_empty"}}
})

-- flint restores full durability on stone equipment as a way to sharpen them
-- the flint isn't consumed on use, it's returned to the player
-- this concept is likely more suitable in scenarios where stone tools are a last resort...
minetest.register_craft({
        type = "shapeless",
        output = "default:sword_stone",
        recipe = {
                "default:sword_stone",
                fli
        },
        replacements = {{fli, fli}}
})

minetest.register_craft({
        type = "shapeless",
        output = "default:shovel_stone",
        recipe = {
                "default:shovel_stone",
                fli
        },
        replacements = {{fli, fli}}
})

minetest.register_craft({
        type = "shapeless",
        output = "default:pick_stone",
        recipe = {
                "default:pick_stone",
                fli
        },
        replacements = {{fli, fli}}
})

minetest.register_craft({
        type = "shapeless",
        output = "default:axe_stone",
        recipe = {
                "default:axe_stone",
                fli
        },
        replacements = {{fli, fli}}
})

minetest.register_craft({
        type = "shapeless",
        output = "farming:hoe_stone",
        recipe = {
                "farming:hoe_stone",
                fli
        },
        replacements = {{fli, fli}}
})
--
-- end of shapeless
--

--
-- gravel-sand-clay
--
minetest.register_craft({
        output = "default:gravel 5",
        recipe = {
                {"default:cobble", "", "default:cobble"}, -- group:stone might work too
                {"", "default:cobble", ""},
                {"default:cobble", "", "default:cobble"}
        }
})

minetest.register_craft({
        output = "default:sand 5",
        recipe = {
                {"default:gravel", "", "default:gravel"},
                {"", "default:gravel", ""},
                {"default:gravel", "", "default:gravel"},
        }
})

minetest.register_craft({
        output = "default:clay 5",
        recipe = {
                {"default:sand", "", "default:sand"}, -- sand, otherwise use group:falling_node
                {"", "default:sand", ""},
                {"default:sand", "", "default:sand"},
        }
})
--
-- end of gravel-sand-clay
--


--
-- dirty
--
for a = 1, 5 do
minetest.register_craft({
        output = "default:dirt_with_grass",
        recipe = {
                { "default:grass_" .. a },
                {"default:dirt"}
        }
})

minetest.register_craft({
        output = "default:dirt_with_dry_grass",
        recipe = {
                {"default:dry_grass_" .. a },
                {"default:dirt"}
        }
})

minetest.register_craft({
        output = "default:dry_dirt_with_dry_grass",
        recipe = {
                {"default:dry_grass_" .. a },
                {"default:dry_dirt"}
        }
})
end

minetest.register_craft({
		output = "default:dirt_with_snow",
		recipe = {
				{"default:snow"},
				{"default:dirt"}
		}
})

minetest.register_craft({
        output = "default:dirt_with_grass",
        recipe = {
                {"default:junglegrass"},
                {"default:dirt"}
        }
})

minetest.register_craft({
        output = "default:dirt_with_rainforest_litter",
        recipe = {
                {"", jgr, ""},
                {jgr, "default:dirt", jgr},
                {"", "bucket:bucket_water", ""}},
        replacements = {{"bucket:bucket_water", "bucket:bucket_empty"}}
})

minetest.register_craft({
        output = "default:dirt_with_rainforest_litter",
        recipe = {
                {"", jgr, ""},
                {jgr, "default:dirt", jgr},
                {"", "bucket:bucket_water", ""}},
        replacements = {{"bucket:bucket_river_water", "bucket:bucket_empty"}}
})
--
-- end of dirty
--


--
-- miscellaneous
--
--if minetest.get_modpath('bucket') then
        minetest.register_craft({
                output = "bucket:bucket_river_water",
                recipe = {
                        {g_l, g_l, g_l},
                        {g_l, g_l, g_l},
                        {"", "bucket:bucket_water", ""}
                }
        })
--end
--
-- end of miscellaneous
--

--
-- renewable_ores
--
minetest.register_craft({
        output = "default:iron_lump 3", -- now players have every reason to find gravel; 9 flint = 3 lumps
        recipe = {
                {"default:flint","default:flint","default:flint"}, -- the idea came from limestone
                {"default:flint","bucket:bucket_water","default:flint"},
                {"default:flint","default:flint","default:flint"}},
        replacements = {{"bucket:bucket_water", "bucket:bucket_empty"}}
})

minetest.register_craft({
        output = "default:iron_lump 3",
        recipe = {
                {"default:flint","default:flint","default:flint"},
                {"default:flint","bucket:bucket_river_water","default:flint"},
                {"default:flint","default:flint","default:flint"}},
        replacements = {{"bucket:bucket_river_water", "bucket:bucket_empty"}}
})

--
-- end of renewable_ores
---

--[[

this section is entirely optional unless the end user wants it
it's left commented out since crafting them seems more logical

--
-- furnace
--
-- stone turns into gravel
minetest.register_craft({
	type = "cooking",
	output = "default:gravel",
	recipe = "group:stone",
})

-- gravel turns into sand
minetest.register_craft({
	type = "cooking",
	output = "default:sand",
	recipe = "default:gravel",
})

-- sand turns into clay
minetest.register_craft({
        type = "cooking",
        output = "default:clay",
        recipe = "default:sand",
})
--
-- end of furnace
--

--]]
