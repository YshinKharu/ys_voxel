# ys_voxel

**ys_voxel** is a collection of custom modifications (mods) for **Minetest**.

This is the `clean` repository and only contains code intended for:

1. playing with and/or pairing together with minetest_game
2. staying simple, minimalistic, and small
3. using already registered and pre-defined functions
4. and, providing more playability value


Get Minetest here:
https://www.minetest.net/downloads/

minetest_game:
https://github.com/minetest/minetest_game
